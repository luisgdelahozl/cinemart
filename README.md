# Cinemart

It is an application where you can find the best rated movies, most popular, those that are currently in the cinema and the next ones to premiere.

## Installation

In order to execute it you must have installed [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/).

Once you download the project, you must rename the files .env.example to .env and php.env.example to php.env.
Complete the parameters in those files. 

In the .env file you could change the web server port (HOST_PORT).

In the php.env file you will have the field CI_ENV where you can put 'development', 'production' or 'testing' (without the quotes) and the field THE_MOVIE_API_KEY where you must place the generated key when registering on the website: https://www.themoviedb.org/.

The final step is to run the `docker-compose up` command in the root folder of the project and wait for all dependencies to be downloaded and installed.

To access the page you must place in your browser the following address `http://localhost:HOST_PORT`.
