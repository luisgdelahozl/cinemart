window.onload = () => {

    const listMoviesImg = document.querySelectorAll('.movies-list .row img');

    function loadImages() {
        const listMovieWidth = listMoviesImg[0].width;
        const appropiateListMovieWidth = getAppropiateImageSize(listMovieWidth, imageConfiguration['poster_sizes']);
        const listMovieBaseUrl = imageConfiguration['base_url'] + appropiateListMovieWidth;
        setImages(listMoviesImg, listMovieBaseUrl, movies, placeholderImage);
    }

    loadImages();

    window.onresize = loadImages;

};