function setImages(imageElements, imagesBaseUrl, imagesName, placeholderImageUrl) {
    for (let i = 0; i < imageElements.length; i++) {
        imageElements[i].src = imagesBaseUrl + imagesName[i];
        imageElements[i].onerror = function () { this.onerror = null; this.src = placeholderImageUrl; };
    }
}

function getAppropiateImageSize(containerWidth, availableSizes) {

    const numberPattern = /\d+/g;
    let appropiateWidth = containerWidth;

    for (let i = 0; i < availableSizes.length; i++) {
        const width = availableSizes[i].match(numberPattern);
        if (width > appropiateWidth || i + 1 === availableSizes.length) {
            appropiateWidth = availableSizes[i];
            break;
        }
    }

    return appropiateWidth;
}