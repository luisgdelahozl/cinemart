window.onload = () => {

    let isScrollLoaded = true;
    let currentPage = page;

    const progressBar = document.querySelector('.progress-line');
    const moviesContainer = document.querySelector('.movies-grid');
    const gridMoviesImg = moviesContainer.querySelectorAll('img');
    let appropiateGridMovieWidth;

    function loadImages() {
        const gridMovieWidth = gridMoviesImg[0].width;
        appropiateGridMovieWidth = getAppropiateImageSize(gridMovieWidth, imageConfiguration['poster_sizes']);
        const gridMovieBaseUrl = imageConfiguration['base_url'] + appropiateGridMovieWidth;
        setImages(gridMoviesImg, gridMovieBaseUrl, movies, placeholderImage);
    }

    loadImages();

    window.onresize = loadImages;

    function createMovieElement() {
        const movieElement = document.createElement('div');
        movieElement.classList.add('col-lg-2', 'col-md-3', 'col-sm-4', 'col-6');
        const cardFlip = document.createElement('div');
        cardFlip.classList.add('card-flip');

        const cardFront = document.createElement('div');
        cardFront.classList.add('card-front', 'flex-column');
        const imgLink = document.createElement('a');
        imgLink.target = '_blank';
        const img = document.createElement('img');
        img.classList.add('w-100');
                            
        imgLink.appendChild(img);
        cardFront.appendChild(imgLink);
        cardFlip.appendChild(cardFront);
        
        const cardBack = document.createElement('div');
        cardBack.classList.add('card-back', 'text-white');
        const cardBody = document.createElement('div');
        cardBody.classList.add('card-body');
        const cardTitle = document.createElement('h4');
        cardTitle.classList.add('card-title');
        const cardText = document.createElement('p');
        cardText.classList.add('card-text');
        const moreInfoLink = document.createElement('a');
        moreInfoLink.classList.add('btn', 'btn-outline-primary');
        moreInfoLink.innerHTML = 'More Info';
        moreInfoLink.target = '_blank';

        cardBody.appendChild(cardTitle);
        cardBody.appendChild(cardText);
        cardBody.appendChild(moreInfoLink);
        cardBack.appendChild(cardBody);
        cardFlip.appendChild(cardBack);

        movieElement.appendChild(cardFlip);

        return movieElement;
    }

    const movieElement = createMovieElement();

    $(window).scroll(function () {
        if (isScrollLoaded && $(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
            isScrollLoaded = false;
            const nextPage = currentPage + 1;
            if (currentPage < totalPages) {
                progressBar.style.display = 'flex';
                fetch(url + '?page=' + nextPage)
                    .then(response => response.json())
                    .then(data => {
    
                        currentPage = nextPage;
                        
                        data.movies.results.forEach((movie) => {
                            let movieElementCopy = movieElement.cloneNode(true);
                            
                            const a = movieElementCopy.querySelector('.card-front a');
                            a.href = detailsUrl.replace('{movie_id}', movie.id);

                            const img = movieElementCopy.querySelector('.card-front img');
                            img.src = imageConfiguration['base_url'] + appropiateGridMovieWidth +  movie.poster_path;
                            img.onerror = function() { this.onerror = null; this.src = placeholderImage; };
                            img.alt = movie.title;

                            const title = movieElementCopy.querySelector('.card-back .card-title');
                            title.innerHTML = movie.title;

                            if(movie.release_date && movie.release_date.length) {
                                const smallText = document.createElement('small');
                                smallText.innerHTML = ' (' + parseInt(movie.release_date) + ')';
                                title.appendChild(smallText);
                            }

                            if(movie.vote_average && movie.vote_average !== 0) {
                                const voteIcon = document.createElement('span');
                                voteIcon.classList.add('fa', 'fa-star', 'checked', 'rating');
                                const cardText = movieElementCopy.querySelector('.card-back .card-text');
                                cardText.appendChild(voteIcon);
                                voteIcon.append(' ' + movie.vote_average);
                            }

                            const moreInfoLink = movieElementCopy.querySelector('.card-back .card-body .btn');
                            moreInfoLink.href = a.href;

                            moviesContainer.appendChild(movieElementCopy);
                            movies.push(movie.poster_path);
                        });
    
                    })
                    .catch(error => {
                        console.error(error);
                    }).then(() => {
                        isScrollLoaded = true;
                        progressBar.style.display = 'none';
                    });
            } else {
                isScrollLoaded = true;
            }
        }
    });
}
