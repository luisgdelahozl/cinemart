window.onload = () => {

    const gridMoviesImg = document.querySelectorAll('.movies-grid img');
    const upcomingMoviesImg = document.querySelectorAll('.movies-group.upcoming img');
    const topRatedMoviesImg = document.querySelectorAll('.movies-group.top-rated img');
    const popularMoviesImg = document.querySelectorAll('.movies-group.popular img');

    function loadImages() {

        const gridMovieWidth = gridMoviesImg[0].width;
        const appropiateGridMovieWidth = getAppropiateImageSize(gridMovieWidth, imageConfiguration['poster_sizes']);
        const gridMovieBaseUrl = imageConfiguration['base_url'] + appropiateGridMovieWidth;

        setImages(gridMoviesImg, gridMovieBaseUrl, nowPlayingMovies, placeholderImage);

        const groupMovieImgWidth = popularMoviesImg[0].width;
        const appropiateGroupMovieWidth = getAppropiateImageSize(groupMovieImgWidth, imageConfiguration['poster_sizes']);
        const groupMovieBaseUrl = imageConfiguration['base_url'] + appropiateGroupMovieWidth;

        setImages(upcomingMoviesImg, groupMovieBaseUrl, upcomingMovies, placeholderImage);
        setImages(popularMoviesImg, groupMovieBaseUrl, popularMovies, placeholderImage);
        setImages(topRatedMoviesImg, groupMovieBaseUrl, topRatedMovies, placeholderImage);

    }

    loadImages();

    window.onresize = loadImages;

}