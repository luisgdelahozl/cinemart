<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <?php include(APPPATH . 'views/partials/_css.php'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/movies.min.css'); ?>">
</head>

<body class="d-flex flex-column">

    <?php include(APPPATH . 'views/partials/_header.php'); ?>

    <main role="main">

        <div class="container-fluid">
            <div class="row movies-grid">
                <?php foreach ($movies['results'] as $index => $movie) { ?>
                    <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                        <div class="card-flip">
                            <div class="card-front flex-column">
                                <a href="<?php echo site_url('movies/' . $movie['id']); ?>">
                                    <img class="w-100" src="<?php echo $image_configuration['base_url']; ?>/w92<?php echo $movie['poster_path']; ?>" alt="<?php echo $movie['title']; ?>" />
                                </a>
                            </div>
                            <div class="card-back text-white">
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <?php echo $movie['title']; ?>
                                        <?php if (!empty($movie['release_date'])) { ?>
                                            <small>(<?php echo date('Y', strtotime($movie['release_date'])); ?>)</small>
                                        <?php } ?>
                                    </h4>
                                    <p class="card-text">
                                        <?php if(!empty($movie['vote_average'])) { ?>
                                        <span class="fa fa-star checked rating"></span> <strong><?php echo $movie['vote_average']; ?></strong>
                                        <?php } ?>
                                    </p>
                                    <a class="btn btn-outline-primary" href="<?php echo site_url('movies/' . $movie['id']); ?>" target="_blank">More Info</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </main>

    <div class="progress-line"></div>

    <?php include(APPPATH . 'views/partials/_footer.php'); ?>
    <?php include(APPPATH . 'views/partials/_js.php'); ?>

    <script>
        const movies = <?php echo json_encode(array_column($movies['results'], 'poster_path')); ?>;
        const imageConfiguration = <?php echo json_encode($image_configuration); ?>;
        const page = <?php echo $movies['page']; ?>;
        const totalResults = <?php echo $movies['total_results']; ?>;
        const totalPages = <?php echo $movies['total_pages']; ?>;
        const url = "<?php echo $fetch_url; ?>";
        const placeholderImage = "<?php echo base_url('assets/img/poster_not_available.png'); ?>";
        const detailsUrl = "<?php echo site_url('movies/{movie_id}'); ?>";
    </script>

    <script src="<?php echo base_url('public/js/images.min.js'); ?>"></script>
    <script src="<?php echo base_url('public/js/movies.min.js'); ?>"></script>
</body>

</html>