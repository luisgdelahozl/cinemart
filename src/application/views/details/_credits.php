<div class="container">
    <?php if (count($movie['credits']['crew'])) { ?>
        <h4 class="mt-3 mb-3">Featured Crew</h4>
        <div class="row">
            <?php foreach ($movie['credits']['crew'] as $index => $crew) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 col-6 p-2">
                    <div class="card">
                        <img class="card-img-top w-100" src="<?php echo !(empty($crew['profile_path'])) ? 'http://image.tmdb.org/t/p/w185' . $crew['profile_path'] : base_url('assets/img/profile.png'); ?>" alt="<?php echo $crew['name'] . ' / ' . $crew['job']; ?>" />
                        <div class="card-body">
                            <h6><?php echo $crew['name']; ?></h6>
                            <p class="card-text"><?php echo $crew['job']; ?></p>
                        </div>
                    </div>
                </div>
                <?php if ($index === 4) break;
            } ?>
        </div>
    <?php } ?>

    <?php if (count($movie['credits']['cast'])) { ?>
        <h4 class="mt-3 mb-3">Top Billed Cast</h4>
        <div class="row">
            <?php foreach ($movie['credits']['cast'] as $index => $cast) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 col-6 p-2">
                    <div class="card">
                        <img class="card-image-top w-100" src="http://image.tmdb.org/t/p/w185<?php echo $cast['profile_path']; ?>" alt="<?php echo $cast['name'] . ' / ' . $cast['character']; ?>" />
                        <div class="card-body">
                            <h6><?php echo $cast['name']; ?></h6>
                            <p class="card-text"><?php echo $cast['character']; ?></p>
                        </div>
                    </div>
                </div>
                <?php if ($index === 4) break;
            } ?>
        </div>
    <?php } ?>
</div>