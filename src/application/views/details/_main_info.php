<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xs-4">
            <img class="d-block mx-auto poster rounded" src="<?php echo $image_configuration['base_url']; ?>w342<?php echo $movie['poster_path']; ?>" alt="<?php echo $movie['title']; ?>" />
        </div>
        <div class="col-lg-9 col-md-9 col-xs-8 mt-4 mt-sm-4 mt-md-0">
            <h2><?php echo $movie['title']; ?>
                <?php if (!empty($movie['vote_average'])) { ?>
                    <small>
                        <span class="checked fa fa-star rating"></span> <strong><?php echo $movie['vote_average']; ?></strong>
                    </small>
                <?php } ?>
            </h2>
            <div class="mt-4">
                <h5>Original Title: <span class="lead"><?php echo $movie['original_title']; ?></span></h5>
                <h5>Release Date: <span class="lead"><?php echo (!empty($movie['release_date']) ? $movie['release_date'] : 'Not available'); ?></span></h5>
                <h5>Duration: <span class="lead"><?php echo (!empty($movie['runtime']) ? $movie['runtime'] . ' minutes' : 'Not available'); ?></span></h5>
                <h5>Homepage: <?php if (!empty($movie['homepage'])) { ?>
                        <a href="<?php echo $movie['homepage']; ?>" target="_blank"><span class="lead"><?php echo $movie['homepage']; ?></span></a>
                    <?php } else { ?>
                        <span class="lead">Not available</span>
                    <?php } ?>
                </h5>
                <h5>Genres: <?php if (count($movie['genres'])) { ?>
                        <span class="lead"><?php echo implode(' | ', array_column($movie['genres'], 'name')); ?></span>
                    <?php } else { ?>
                        <span class="lead">Not available</span>
                    <?php } ?>
                </h5>
                <p><?php echo $movie['overview']; ?></p>
                <?php if (!empty($movie['external_ids'])) { ?>
                    <div class="social-icons">
                        <?php if (!empty($movie['external_ids']['imdb_id'])) { ?>
                            <a class="text-white" href="<?php echo $this->config->item('imdb_url') . $movie['external_ids']['imdb_id']; ?>" target="_blank"><i class="fab fa-imdb"></i></a>
                        <?php } ?>
                        <?php if (!empty($movie['external_ids']['facebook_id'])) { ?>
                            <a class="text-white" href="<?php echo $this->config->item('facebook_url') . $movie['external_ids']['facebook_id']; ?>" target="_blank"><i class="fab fa-facebook"></i></a>
                        <?php } ?>
                        <?php if (!empty($movie['external_ids']['instagram_id'])) { ?>
                            <a class="text-white" href="<?php echo $this->config->item('instagram_url') . $movie['external_ids']['instagram_id']; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                        <?php } ?>
                        <?php if (!empty($movie['external_ids']['twitter_id'])) { ?>
                            <a class="text-white" href="<?php echo $this->config->item('twitter_url') . $movie['external_ids']['twitter_id']; ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>