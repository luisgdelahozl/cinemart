<div class="container">
    <h4 class="mt-3 mb-3">Similar Movies</h4>
    <div class="row">
        <?php foreach ($movie['similar']['results'] as $index => $similar) { ?>
            <div class="col-lg-2 col-md-3 col-sm-4 col-6 p-2">
                <div class="card">
                    <a href="<?php echo site_url('movies/' . $similar['id']); ?>" target="_blank">
                        <img class="d-block mx-auto rounded w-100" src="<?php echo $image_configuration['base_url']; ?>w342<?php echo $similar['poster_path']; ?>" alt="<?php echo $similar['title']; ?>" />
                    </a>
                </div>
            </div>
            <?php if ($index === 4) break;
        } ?>
    </div>
</div>