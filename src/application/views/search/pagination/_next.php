<?php if ($movies['page'] !== $movies['total_pages']) { ?>
    <li class="page-item">
        <a class="page-link" href="<?php echo site_url('movies/search?keyword=' . $keyword . '&page=' . ($movies['page'] + 1)); ?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
        </a>
    </li>
<?php } ?>