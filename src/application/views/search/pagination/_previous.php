<?php if ($movies['page'] !== 1) { ?>
    <li class="page-item">
        <a class="page-link" href="<?php echo site_url('movies/search?keyword=' . $keyword . '&page=' . ($movies['page'] - 1)); ?>" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
        </a>
    </li>
<?php } ?>