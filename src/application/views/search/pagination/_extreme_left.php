<?php for ($i = 1; $i <= 2 && $i <= $movies['total_pages']; $i++) {
    $lastNumberAdded = $i; ?>
    <li class="page-item <?php echo ($movies['page'] === $i) ? 'active' : '' ?>">
        <a class="page-link" href="<?php echo ($movies['page'] !== $i) ? site_url('movies/search?keyword=' . $keyword . '&page=' . $i) : '#'; ?>"><?php echo $i; ?></a>
    </li>
<?php } ?>