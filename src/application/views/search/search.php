<?php

defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <?php include(APPPATH . 'views/partials/_css.php'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/main.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/search.min.css'); ?>">

</head>

<body class="d-flex flex-column">

    <?php include(APPPATH . 'views/partials/_header.php'); ?>

    <main role="main">

        <div class="container movies-list">

            <?php if (isset($error)) { ?>
                <h3 class="my-4">Error: <?php echo $error; ?></h3>
            <?php } else { ?>
                <h3 class="my-4">Search: <span><?php echo htmlentities($keyword); ?></span>
                    <small> > <?php echo $movies['total_results']; ?> result(s) found</small>
                </h3>

                <?php foreach ($movies['results'] as $movie) { ?>
                    <div class="row">
                        <div class="col-4 col-sm-4 col-md-2 col-lg-2">
                            <a href="<?php echo site_url('movies/' . $movie['id']); ?>">
                                <img class="rounded mb-3 mb-md-0" src="<?php echo $image_configuration['base_url']; ?>/w92<?php echo $movie['poster_path']; ?>" alt="<?php echo $movie['title']; ?>" width="100%" />
                            </a>
                        </div>
                        <div class="col-8 col-sm-8 col-md-10 col-lg-10">
                            <h4><?php echo $movie['title']; ?> <small><span class="fa fa-star checked rating"></span> <strong><?php echo $movie['vote_average']; ?></strong> </small></h4>
                            <p class="block-ellipsis text-justify"><?php echo $movie['overview']; ?></p>
                            <a class="btn btn-primary float-right" href="<?php echo site_url('movies/' . $movie['id']); ?>">More Info</a>
                        </div>
                    </div>
                    <hr />
                <?php } ?>

                <?php if ($movies['total_pages'] !== 1) include(APPPATH . 'views/search/_pagination.php'); ?>

                <script>
                    const movies = <?php echo json_encode(array_column($movies['results'], 'poster_path')); ?>;
                    const imageConfiguration = <?php echo json_encode($image_configuration); ?>;
                    const placeholderImage = "<?php echo base_url('assets/img/poster_not_available.png'); ?>";
                </script>

            <?php } ?>

        </div>

    </main>

    <?php include(APPPATH . 'views/partials/_footer.php'); ?>
    <?php include(APPPATH . 'views/partials/_js.php'); ?>

    <script type="text/javascript" src="<?php echo base_url('public/js/images.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/js/search.min.js'); ?>"></script>
</body>

</html>