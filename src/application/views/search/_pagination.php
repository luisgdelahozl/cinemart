<ul class="pagination justify-content-center flex-wrap">
    <?php include(APPPATH . 'views/search/pagination/_previous.php'); ?>


    <?php if ($movies['total_pages'] <= 8) for ($i = 1; $i <= $movies['total_pages']; $i++) { ?>
        <li class="page-item <?php echo ($movies['page'] === $i) ? 'active' : '' ?>">
            <a class="page-link" href="<?php echo site_url('movies/search?keyword=' . $keyword . '&page=' . $i); ?>"><?php echo $i; ?></a>
        </li>
    <?php } else { ?>

        <?php $lastNumberAdded = 1; ?>

        <?php include(APPPATH . 'views/search/pagination/_extreme_left.php'); ?>

        <?php if ($movies['page'] > 6) { ?>
            <li class="page-item"><a class="page-link"><strong>...</strong></a></li>
        <?php } else { ?>
            <?php for ($i = $lastNumberAdded + 1; $i <= $movies['page'] + 3 && $i <= $movies['total_pages']; $i++) {
                $lastNumberAdded = $i; ?>
                <li class="page-item <?php echo ($movies['page'] === $i) ? 'active' : '' ?>">
                    <a class="page-link" href="<?php echo ($movies['page'] !== $i) ? site_url('movies/search?keyword=' . $keyword . '&page=' . $i) : '#'; ?>"><?php echo $i; ?></a>
                </li>
            <?php } ?>
        <?php } ?>

        <?php for ($i = $movies['page'] - 2; $i > $lastNumberAdded && $i <= $movies['page'] + 2 && $i <= $movies['total_pages']; $i++) {
            $lastNumberAdded = $i; ?>
            <li class="page-item <?php echo ($movies['page'] === $i) ? 'active' : '' ?>">
                <a class="page-link" href="<?php echo ($movies['page'] !== $i) ? site_url('movies/search?keyword=' . $keyword . '&page=' . $i) : '#'; ?>"><?php echo $i; ?></a>
            </li>
        <?php } ?>

        <?php if ($movies['total_pages'] - $movies['page'] > 5) { ?>
            <li class="page-item"><a class="page-link"><strong>...</strong></a></li>
        <?php } else { ?>
            <?php for ($i = $lastNumberAdded + 1; $i <= $movies['page'] + 3 && $i <= $movies['total_pages']; $i++) {
                $lastNumberAdded = $i; ?>
                <li class="page-item <?php echo ($movies['page'] === $i) ? 'active' : '' ?>">
                    <a class="page-link" href="<?php echo ($movies['page'] !== $i) ? site_url('movies/search?keyword=' . $keyword . '&page=' . $i) : '#'; ?>"><?php echo $i; ?></a>
                </li>
            <?php } ?>
        <?php } ?>

        <?php include(APPPATH . 'views/search/pagination/_extreme_right.php'); ?>

    <?php } ?>


    <?php include(APPPATH . 'views/search/pagination/_next.php'); ?>
</ul>