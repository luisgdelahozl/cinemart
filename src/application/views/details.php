<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <?php include(APPPATH . 'views/partials/_css.php'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/details.min.css'); ?>">
</head>

<body class="d-flex flex-column">

    <?php include(APPPATH . 'views/partials/_header.php'); ?>

    <main role="main">

        <div class="main-info py-5 text-white">
            <?php include(APPPATH . 'views/details/_main_info.php'); ?>
        </div>

        <?php if (count($movie['credits']['crew']) || count($movie['credits']['cast'])) { ?>
            <section class="credits mt-5">
                <?php include(APPPATH . 'views/details/_credits.php'); ?>
            </section>
        <?php } ?>

        <?php if (!empty($movie['recommendations']['results']) && count($movie['recommendations']['results'])) { ?>
            <section class="mt-5">
                <?php include(APPPATH . 'views/details/_recommendations.php'); ?>
            </section>
        <?php } ?>

        <?php if (!empty($movie['similar']['results']) && count($movie['similar']['results'])) { ?>
            <section class="py-5">
                <?php include(APPPATH . 'views/details/_similar.php'); ?>
            </section>
        <?php } ?>

    </main>

    <?php include(APPPATH . 'views/partials/_footer.php'); ?>
    <?php include(APPPATH . 'views/partials/_js.php'); ?>

</body>

</html>