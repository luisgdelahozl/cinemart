<footer class="bg-dark py-3">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xs-12 col-sm-8 col-md-9">
                <p class="text-white">&copy; <?php echo date('Y'); ?> Cinemart, Inc. This product uses the TMDb API but is not endorsed or certified by the TMDb.</p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <a  href="https://www.themoviedb.org">
                    <img class="tmdb-logo" src="<?php echo base_url('public/img/powered-by-rectangle-green.svg'); ?>" alt="Powered by The Movie Database" />
                </a>
            </div>
        </div>
    </div>
</footer>