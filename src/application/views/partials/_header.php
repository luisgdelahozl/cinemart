<header>
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="<?php echo site_url('/'); ?>">Cinemart</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?php echo ($active_page === 'home' ? 'active' : ''); ?>">
                    <a class="nav-link" href="<?php echo site_url('/'); ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item <?php echo ($active_page === 'now_playing' ? 'active' : ''); ?>"">
                    <a class=" nav-link" href="<?php echo site_url('movies/now-playing'); ?>">Now Playing</a>
                </li>
                <li class="nav-item <?php echo ($active_page === 'upcoming' ? 'active' : ''); ?>"">
                    <a class=" nav-link" href="<?php echo site_url('movies/upcoming'); ?>">Upcoming</a>
                </li>
                <li class="nav-item <?php echo ($active_page === 'top_rated' ? 'active' : ''); ?>"">
                    <a class=" nav-link" href="<?php echo site_url('movies/top-rated'); ?>">Top Rated</a>
                </li>
                <li class="nav-item <?php echo ($active_page === 'popular' ? 'active' : ''); ?>"">
                    <a class=" nav-link" href="<?php echo site_url('movies/popular'); ?>">Popular</a>
                </li>
            </ul>
            <form class="form-inline mt-2 mt-md-0" method="GET" action="<?php echo site_url('movies/search'); ?>">
                <input class="form-control mr-sm-2" type="text" name="keyword" placeholder="Search" aria-label="Search" value="<?php echo ((!empty($keyword)) ? htmlentities($keyword): ''); ?>" required>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
</header>