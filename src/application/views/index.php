<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <?php include(APPPATH . 'views/partials/_css.php'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/main.min.css'); ?>">

</head>

<body class="d-flex flex-column">

    <?php include(APPPATH . 'views/partials/_header.php'); ?>

    <main role="main">

        <div class="container-fluid">
            <div class="row movies-grid">
                <?php foreach ($movies['now_playing'] as $index => $movie) { ?>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-4">
                        <div class="card">
                            <a href="<?php echo site_url('movies/' . $movie['id']); ?>">
                                <img width="100%" src="<?php echo $image_configuration['base_url']; ?>/w92<?php echo $movie['poster_path']; ?>" alt="<?php echo $movie['title']; ?>" />
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="container movies-group upcoming mt-3">
            <div>
                <h3 class="movies-group-title">Upcoming</h3>
                <a href="<?php echo site_url('movies/upcoming'); ?>" class="float-right">See more</a>
            </div>
            <div class="row flex-row flex-nowrap">
                <?php foreach ($movies['upcoming'] as $index => $movie) { ?>
                    <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                        <div class="card">
                            <a href="<?php echo site_url('movies/' . $movie['id']); ?>">
                                <img width="100%" src="<?php echo $image_configuration['base_url']; ?>/w92<?php echo $movie['poster_path']; ?>" alt="<?php echo $movie['title']; ?>" />
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="container movies-group top-rated mt-3">
            <div>
                <h3 class="movies-group-title">Top Rated</h3>
                <a href="<?php echo site_url('movies/top-rated'); ?>" class="float-right">See more</a>
            </div>
            <div class="row flex-row flex-nowrap">
                <?php foreach ($movies['top_rated'] as $index => $movie) { ?>
                    <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                        <div class="card">
                            <a href="<?php echo site_url('movies/' . $movie['id']); ?>">
                                <img width="100%" src="<?php echo $image_configuration['base_url']; ?>/w92<?php echo $movie['poster_path']; ?>" alt="<?php echo $movie['title']; ?>" />
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="container movies-group popular mt-3">
            <div>
                <h3 class="movies-group-title">Popular</h3>
                <a href="<?php echo site_url('movies/popular'); ?>" class="float-right">See more</a>
            </div>
            <div class="row flex-row flex-nowrap">
                <?php foreach ($movies['popular'] as $index => $movie) { ?>
                    <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                        <div class="card">
                            <a href="<?php echo site_url('movies/' . $movie['id']); ?>">
                                <img width="100%" src="<?php echo $image_configuration['base_url']; ?>/w92<?php echo $movie['poster_path']; ?>" alt="<?php echo $movie['title']; ?>" />
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </main>

    <?php include(APPPATH . 'views/partials/_footer.php'); ?>
    <?php include(APPPATH . 'views/partials/_js.php'); ?>

    <script>
        const imageConfiguration = <?php echo json_encode($image_configuration); ?>;
        const placeholderImage = "<?php echo base_url('assets/img/poster_not_available.png'); ?>";

        const nowPlayingMovies = <?php echo json_encode(array_column($movies['now_playing'], 'poster_path')); ?>;
        const upcomingMovies = <?php echo json_encode(array_column($movies['upcoming'], 'poster_path')); ?>;
        const topRatedMovies = <?php echo json_encode(array_column($movies['top_rated'], 'poster_path')); ?>;
        const popularMovies = <?php echo json_encode(array_column($movies['popular'], 'poster_path')); ?>;
    </script>

    <script src="<?php echo base_url('public/js/images.min.js'); ?>"></script> 
    <script src="<?php echo base_url('public/js/main.min.js'); ?>"></script> 

</body>

</html>