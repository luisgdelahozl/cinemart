<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title ?> - Error </title>
    <?php include(APPPATH . 'views/partials/_css.php'); ?>
    <style>
        body {
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);
        }

        body, html {
            height: 100%;
        }

        .error-template {
            padding: 40px 15px;
            text-align: center;
        }

        .error-actions {
            margin-top: 15px;
            margin-bottom: 15px;
        }
    </style>
</head>

<body>

    <div class="container h-100">
        <div class="row justify-content-center align-items-center h-100">
            <div class="col-md-12">
                <div class="error-template">
                    <h1>Oops!</h1>
                    <h2><?php echo (isset($error) && !empty($error['title']) ? $error['title'] : '404 Not Found'); ?></h2>
                    <div class="error-details">
                        <?php echo (isset($error) && !empty($error['message']) ? $error['message'] : 'Sorry, an error has occured, requested page not found!') ?>
                    </div>
                    <div class="error-actions">
                        <a href="<?php echo (isset($error) && !empty($error['url']) ? $error['url'] : site_url('/')); ?>" class="btn btn-primary btn-lg">
                            <span class="glyphicon glyphicon-home"></span>
                            <?php echo (isset($error) && !empty($error['button_text']) ? $error['button_text'] : 'Take Me Home '); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>