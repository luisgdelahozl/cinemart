<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {

    public function index() {
        $data['title'] = $this->config->item('app_name');
        $this->load->view('error', $data);
    }

}