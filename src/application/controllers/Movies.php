<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Movies extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        redirect('/');
    }

    public function upcoming()
    {
        $movies = $this->api_client->getUpcomingMovies();
        $data['title'] = $this->config->item('app_name') . ' - ' . ' Upcoming Movies';
        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['active_page'] = 'upcoming';
            $data['fetch_url'] = site_url('movies/getUpcoming');
            $data['image_configuration'] = [
				'poster_sizes' => $this->apiConfiguration->images->poster_sizes,
				'base_url' => $_SERVER['REQUEST_SCHEME'] === 'https' ?
					$this->apiConfiguration->images->secure_base_url: 
					$this->apiConfiguration->images->base_url,
			];
            $this->load->view('movies', $data);
        } else {
            $this->load->view('error', $data);
        }
    }

    public function getUpcoming()
    {
        $page = $this->input->get('page');
        $movies = $this->api_client->getUpcomingMovies(['page' => $page]);
        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['configuration'] = $this->apiConfiguration;
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        } else {
            $this->load->view('error', $data);
        }
    }

    public function topRated()
    {
        $movies = $this->api_client->getTopRatedMovies();
        $data['title'] = $this->config->item('app_name') . ' - ' . ' Top Rated Movies';
        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['active_page'] = 'top_rated';
            $data['fetch_url'] = site_url('movies/getTopRated');
            $data['image_configuration'] = [
				'poster_sizes' => $this->apiConfiguration->images->poster_sizes,
				'base_url' => $_SERVER['REQUEST_SCHEME'] === 'https' ?
					$this->apiConfiguration->images->secure_base_url: 
					$this->apiConfiguration->images->base_url,
			];
            $this->load->view('movies', $data);
        } else {
            $this->load->view('error', $data);
        }
    }

    public function getTopRated()
    {
        $page = $this->input->get('page');
        $movies = $this->api_client->getTopRatedMovies(['page' => $page]);
        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['configuration'] = $this->apiConfiguration;
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        } else {
            $this->load->view('error', $data);
        }
    }

    public function nowPlaying()
    {
        $movies = $this->api_client->getNowPlayingMovies();
        $data['title'] = $this->config->item('app_name') . ' - ' . ' Now Playing Movies';
        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['active_page'] = 'now_playing';
            $data['fetch_url'] = site_url('movies/getNowPlaying');
            $data['image_configuration'] = [
				'poster_sizes' => $this->apiConfiguration->images->poster_sizes,
				'base_url' => $_SERVER['REQUEST_SCHEME'] === 'https' ?
					$this->apiConfiguration->images->secure_base_url: 
					$this->apiConfiguration->images->base_url,
			];
            $this->load->view('movies', $data);
        } else {
            $this->load->view('error', $data);
        }
    }

    public function getNowPlaying()
    {
        $page = $this->input->get('page');
        $movies = $this->api_client->getNowPlayingMovies(['page' => $page]);
        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['configuration'] = $this->apiConfiguration;
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        } else {
            $this->load->view('error', $data);
        }
    }

    public function popular()
    {
        $movies = $this->api_client->getPopularMovies();
        $data['title'] = $this->config->item('app_name') . ' - ' . ' Popular Movies';
        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['active_page'] = 'popular';
            $data['fetch_url'] = site_url('movies/getPopular');
            $data['image_configuration'] = [
				'poster_sizes' => $this->apiConfiguration->images->poster_sizes,
				'base_url' => $_SERVER['REQUEST_SCHEME'] === 'https' ?
					$this->apiConfiguration->images->secure_base_url: 
					$this->apiConfiguration->images->base_url,
			];
            $this->load->view('movies', $data);
        } else {
            $this->load->view('error', $data);
        }
    }

    public function getPopular()
    {
        $page = $this->input->get('page');
        $movies = $this->api_client->getPopularMovies(['page' => $page]);
        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['configuration'] = $this->apiConfiguration;
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        } else {
            $this->load->view('error', $data);
        }
    }

    public function getDetails($id)
    {
        $movie = $this->api_client->getMovieDetails($id);
        $data['title'] = $this->config->item('app_name') . ' - ' . 'Movie Details';

        if ($movie['success']) {
            $data['active_page'] = 'details';
            $data['movie'] = $movie;
            $data['image_configuration'] = [
				'poster_sizes' => $this->apiConfiguration->images->poster_sizes,
				'base_url' => $_SERVER['REQUEST_SCHEME'] === 'https' ?
					$this->apiConfiguration->images->secure_base_url: 
					$this->apiConfiguration->images->base_url,
            ];
            $this->load->view('details', $data);
        } else {
            $this->load->view('error', $data);
        }
    }

    public function search() {

        $keyword = $this->input->get('keyword');
        $page = (!empty($this->input->get('page'))) ? $this->input->get('page') : 1;
        $movies = $this->api_client->searchMovies(array('query' => $keyword, 'page' => $page));

        $data['title'] = $this->config->item('app_name') . ' - ' . ' Search Movies';
        $data['active_page'] = 'search';

        if ($movies['success']) {
            $data['movies'] = $movies;
            $data['keyword'] = $keyword;
            $data['image_configuration'] = [
				'poster_sizes' => $this->apiConfiguration->images->poster_sizes,
				'base_url' => $_SERVER['REQUEST_SCHEME'] === 'https' ?
					$this->apiConfiguration->images->secure_base_url: 
					$this->apiConfiguration->images->base_url,
			];
            $this->load->view('search/search', $data);
        } else {
            $data['error'] = $movies['errors'][0];
            $this->load->view('search/search', $data);
        }
    }
}
