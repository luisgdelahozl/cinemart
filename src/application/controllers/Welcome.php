<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	const MAX_NOW_PLAYING_MOVIES_TO_SHOW = 12;
	const MAX_OTHER_MOVIES_CATEGORIES_TO_SHOW = 8;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('movie');
	}	

	public function index()
	{	
		$movies = $this->api_client->getMainMovies();
		
		$data = [];
		$data['title'] = $this->config->item('app_name');

		if($movies) {

			$movies['now_playing'] = array_slice($movies['now_playing']['results'], 0, self::MAX_NOW_PLAYING_MOVIES_TO_SHOW); 
			$movies['top_rated'] = array_slice($movies['top_rated']['results'], 0, self::MAX_OTHER_MOVIES_CATEGORIES_TO_SHOW); 
			$movies['upcoming'] = array_slice($movies['upcoming']['results'], 0, self::MAX_OTHER_MOVIES_CATEGORIES_TO_SHOW); 
			$movies['popular'] = array_slice($movies['popular']['results'], 0, self::MAX_OTHER_MOVIES_CATEGORIES_TO_SHOW); 

			foreach($movies as $key => $movie) {
				$movies[$key] = array_map("filterMoviePrimaryInfo", $movie);
			}

			$data['movies'] = $movies;
			$data['active_page'] = 'home'; 
			$data['image_configuration'] = [
				'poster_sizes' => $this->apiConfiguration->images->poster_sizes,
				'base_url' => $_SERVER['REQUEST_SCHEME'] === 'https' ?
					$this->apiConfiguration->images->secure_base_url: 
					$this->apiConfiguration->images->base_url,
			];

			$this->load->view('index', $data);
		} else {
			$data['error'] = [
				'title' => 'Unknown error',
				'message' => 'Sorry, an error has occured. Please try again.',
				'url' => site_url('/'),
				'button_text' => 'Try again',
			];
			$this->load->view('error', $data);
		}
	}
}
