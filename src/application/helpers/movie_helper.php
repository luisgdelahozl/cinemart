<?php
defined('BASEPATH') or exit('No direct script access allowed');

function filterMoviePrimaryInfo($movie) {
    return [
        'id' => $movie['id'],
        'poster_path' => $movie['poster_path'],
        'title' => $movie['title'],
    ];
}