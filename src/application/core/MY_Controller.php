<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $apiConfiguration;

    public function __construct()
    {
        parent::__construct();
        $apiKey = getenv('THE_MOVIE_DB_API_KEY');
        $this->load->library('api_client', array('api_key' => $apiKey));
        $this->load->library('api_configuration', array('api_client' => $this->api_client));
        $this->apiConfiguration = $this->api_configuration->get();
        
        if(!$this->apiConfiguration) {
            $data = ['title' => $this->config->item('app_name'), 'error' => [
				'title' => 'Unknown error',
				'message' => 'Sorry, an error has occured. Please try again.',
				'url' => site_url('/'),
				'button_text' => 'Try again',
            ]];

            echo $this->load->view('error', $data, TRUE);
            exit;
        }
    }

}
