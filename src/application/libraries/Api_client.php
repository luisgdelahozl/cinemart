<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;

class Api_Client
{

    const BASE_URL = 'https://api.themoviedb.org/3/';
    const GET_UPCOMING_MOVIES = 'movie/upcoming';
    const GET_TOP_RATED_MOVIES = 'movie/top_rated';
    const GET_POPULAR_MOVIES = 'movie/popular';
    const GET_NOW_PLAYING_MOVIES = 'movie/now_playing';
    const GET_CONFIGURATION = 'configuration';
    const GET_MOVIE_DETAILS = 'movie/{movie_id}';
    const SEARCH_MOVIES = 'search/movie';

    function __construct($params)
    {
        $this->client = new Client(['base_uri' => self::BASE_URL]);
        $this->apiKey = $params['api_key'];
    }

    function getMainMovies($query = [])
    {
        $parameters = $this->generateRequestParameters($query);

        $requests = [
            'upcoming' => $this->client->getAsync(self::GET_UPCOMING_MOVIES, $parameters),
            'top_rated' => $this->client->getAsync(self::GET_TOP_RATED_MOVIES, $parameters),
            'popular' => $this->client->getAsync(self::GET_POPULAR_MOVIES, $parameters),
            'now_playing' => $this->client->getAsync(self::GET_NOW_PLAYING_MOVIES, $parameters),
        ];

        try {
            $response = [];
            $results = Promise\unwrap($requests);
            foreach ($results as $index => $result) {
                $response[$index] = json_decode($result->getBody(), TRUE);
            }
            return $response;
        } catch (RequestException $e) {
            log_message('error', $e->getMessage());
            return FALSE;
        }
    }

    public function getConfiguration()
    {
        $parameters = $this->generateRequestParameters();

        try {
            $response = $this->client->get(self::GET_CONFIGURATION, $parameters);
            return json_decode($response->getBody(), TRUE);
        } catch (RequestException $e) {
            log_message('error', $e->getMessage());
            return FALSE;
        }
    }

    public function getUpcomingMovies($query = [])
    {
        $parameters = $this->generateRequestParameters($query);

        return $this->executeGetRequest(self::GET_UPCOMING_MOVIES, $parameters);
    }

    public function getTopRatedMovies($query = [])
    {
        $parameters = $this->generateRequestParameters($query);

        return $this->executeGetRequest(self::GET_TOP_RATED_MOVIES, $parameters);
    }

    public function getNowPlayingMovies($query = [])
    {
        $parameters = $this->generateRequestParameters($query);

        return $this->executeGetRequest(self::GET_NOW_PLAYING_MOVIES, $parameters);
    }

    public function getPopularMovies($query = [])
    {
        $parameters = $this->generateRequestParameters($query);

        return $this->executeGetRequest(self::GET_POPULAR_MOVIES, $parameters);
    }

    public function getMovieDetails($id)
    {
        $url = str_replace('{movie_id}', $id, self::GET_MOVIE_DETAILS);
        $parameters = $this->generateRequestParameters([
            'append_to_response' => 'credits,external_ids,recommendations,similar',
        ]);

        return $this->executeGetRequest($url, $parameters);
    }

    public function searchMovies($query)
    {
        $parameters = $this->generateRequestParameters($query);
        
        return $this->executeGetRequest(self::SEARCH_MOVIES, $parameters);
    }

    private function generateRequestParameters($query = [])
    {
        $query['api_key'] = $this->apiKey;
        $parameters = ['query' => $query];
        return $parameters;
    }

    private function executeGetRequest($url, $parameters)
    {
        try {
            $response = $this->client->get($url, $parameters);
            $response = json_decode($response->getBody(), TRUE);
            $response['success'] = TRUE;
        } catch (RequestException $e) {
            log_message('error', $e->getMessage());
            if ($e->hasResponse()) {
                $response = json_decode($e->getResponse()->getBody(), TRUE);
                $response['http_code'] = $e->getResponse()->getStatusCode();
            } else {
                $response['http_code'] = $e->getCode();
            }
            $response['success'] = FALSE;
        }

        return $response;
    }
}
