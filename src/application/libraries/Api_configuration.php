<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Api_configuration
{

    const CONFIGURATION_FILEPATH = APPPATH . 'config/api_configuration.json';

    public function __construct($params)
    {
        $this->apiClient = $params['api_client'];
    }

    public function get()
    {
        $configuration = array();
        $needUpdate = FALSE;

        if (file_exists(self::CONFIGURATION_FILEPATH)) {
            $configurationFileExistenceTimeInDays = filemtime(self::CONFIGURATION_FILEPATH) / 60 / 60 / 24;
            if (strtotime(('1+ day')) - $configurationFileExistenceTimeInDays >= 1) {
                $needUpdate = TRUE;
            } else {
                $configuration = json_decode(file_get_contents(self::CONFIGURATION_FILEPATH));
            }
        } else {
            $needUpdate = TRUE;
        }

        if ($needUpdate) {
            $configuration = $this->apiClient->getConfiguration();
            if($configuration && count($configuration)) {
                $wasSaved = $this->save($configuration);
                $configuration = $wasSaved ? json_decode(json_encode($configuration)) : FALSE;
            }
        }

        return $configuration;
    }

    private function save($configuration)
    {
        return file_put_contents(self::CONFIGURATION_FILEPATH, json_encode($configuration));
    }
}
